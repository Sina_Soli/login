import {Avatar, Grid, Paper,TextField,FormControlLabel,Button} from '@mui/material';
import LoginRoundedIcon from '@mui/icons-material/LoginRounded';
import Checkbox from '@mui/material/Checkbox';
import { useFormik } from 'formik';
import * as Yup from 'yup';


function App() {
  const formik = useFormik({
    initialValues:{
      Email:"",
      Password:"", 
    },

    validationSchema:Yup.object({ 
      Email:Yup.string().email("Invalid Email"),
      Password : Yup.string().required("Enter password!!!")
    }),



    onSubmit: (values) => { console.log(values);},

  })
  
  return (
    <form onSubmit={formik.handleSubmit}>
    <Grid>
      <Paper elevation={10} style={{width:"50%",height:"70vh",margin:"20px auto"}} >
        <Grid align="center">
       <Avatar style={{backgroundColor:"#4caf50"}}> <LoginRoundedIcon/>  </Avatar>
       <h2>Irisa Login Form</h2>
       </Grid>

       <Grid style={{margin:"0 20px 0 20px"}}>
             <TextField value={formik.values.Email} style={{marginBottom:"20px",}} type={'email'}   label="Email : info@irisaco.com" variant="outlined" id='Email' onChange={formik.handleChange} onBlur={formik.handleBlur} fullWidth  />
             {formik.touched.Email && formik.errors.Email ? <p style={{color:"red"}}>{formik.errors.Email}</p> : null }
             <TextField value={formik.values.Password}  label="Password" type={'password'} variant="outlined" id='Password' onChange={formik.handleChange} onBlur={formik.handleBlur} fullWidth  />
             {formik.touched.Password && formik.errors.Password ? <p style={{color:"red"}}>{formik.errors.Password}</p> : null }
       </Grid>

       <FormControlLabel style={{align:"left",marginLeft:"10px"}} control={<Checkbox defaultChecked />} label="Forget my pass!" />
       <Grid style={{margin:"0 20px 0 20px"}}>
       <Button  variant="contained" type='submit'   fullWidth>Login</Button>
       </Grid>
      </Paper>
    </Grid>
    </form>
  );
}

export default App;
